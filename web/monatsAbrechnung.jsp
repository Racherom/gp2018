<%-- 
    Document   : monatsAbrechnung
    Created on : 18.06.2018, 10:25:42
    Author     : christoph.reisch
--%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="DAO.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%!
    Mitglied mitglied;
    List<Aufenthalt> aufenthalte;
%>
<%
    mitglied = (Mitglied) session.getAttribute("mitglied");
    aufenthalte = (ArrayList<Aufenthalt>) session.getAttribute("aufenthalte");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Monatsbezogene Abrechnung</title>
        <style>
            table {
                font-family: arial;
                border-collapse: collapse;
                width: 100%;
            }
            td, th {
                border: 1px solid #dddddd;
                text-align: left;
                padding: 8px;
            }

            tr:nth-child(even) {
                background-color: #dddddd;
            }
            .pointer {cursor: pointer;}
        </style>
    </head>
    <body>
        <%
            String str_Jahr = request.getParameter("jahr");
            String str_Monat = request.getParameter("monat");
            //out.println(str_Monat);
            int Jahr = Integer.parseInt(str_Jahr);
            int Monat = Integer.parseInt(str_Monat);
        %>
        <h3>Monatsbezogene Abrechnung <% out.print(Aufenthalt.monatsNamen[Monat-1]); out.print(" "); out.print(Jahr); %>;</h3>
        <table>
            <tr>
                <th>Ankunftszeit</th>
                <th>Endzeit</th>
                <th>Stunden</th>
            </tr>


            <%
                List<Aufenthalt> Test3 = Aufenthalt.getMonth(aufenthalte, Jahr, Monat);
                for (int i = 0; i < Test3.size(); i++) {
                    Aufenthalt a = Test3.get(i);
            %>
            <tr>
                <td><%out.println(a.getVon());%></td>
                <td><%out.println(a.getBis());%></td>
                <td><%out.println(a.getStunden());%></td>
            </tr>
            <%
                }
            %>

        </table>
        <h3>Gesamt Betrag: <% out.print(Aufenthalt.abrechnung(Test3));%> €</h3>
        <button type="button" onclick="parent.location = 'http://localhost:8080/gp2018/index.jsp'">Abmelden</button>
    </body>
</html>
