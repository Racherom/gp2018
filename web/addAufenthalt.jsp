<%-- 
    Document   : addAufenthalt
    Created on : 06.07.2018, 04:06:02
    Author     : Racherom
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="DAO.*"%>

<%
    MYSQLDAO sqlDao = new MYSQLDAO();
    
    String von = request.getParameter("von");
    String bis = request.getParameter("bis");
    String id_str = request.getParameter("id");
    int id = Integer.parseInt(id_str);
    
    sqlDao.setAufenthalt(new Aufenthalt(von, bis, id));

%>

<meta http-equiv="refresh" content="0, URL=http://localhost:8080/gp2018/newAufenthalt.jsp">
