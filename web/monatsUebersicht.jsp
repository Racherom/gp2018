<%-- 
    Document   : Übersicht
    Created on : 18.06.2018, 09:42:59
    Author     : christoph.reisch
--%>

<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="DAO.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%! 
    Mitglied mitglied;
    List<Aufenthalt> aufenthalte;
    %>
<%
   mitglied = (Mitglied)session.getAttribute("mitglied");
   aufenthalte = (ArrayList<Aufenthalt>)session.getAttribute("aufenthalte");
%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Monatsübersicht</title>
        <style>
        table {
            font-family: arial;
            border-collapse: collapse;
            width: 100%;
        }
        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }
        tr:nth-child(even) {
            background-color: #dddddd;
        }
        .pointer {cursor: pointer;}
        </style>
    </head>
    <body>

        <h3>Monatsübersicht</h3>
        <table>
            <tr>
                <th>Jahr</th>
                <th>Monat</th>
                <th>Stunden</th>
                <th>Betrag</th>
            </tr>
            
            <%
                for(int j=2016; j<2019; j++){
                for (int m=1;m<=12;m++){
                    List<Aufenthalt> Test3 = Aufenthalt.getMonth(aufenthalte, j, m);
                    float s = Aufenthalt.stunden(Test3);
                    if(s > 0 ){                    

            %>
            <tr class="pointer" onclick="parent.location='http://localhost:8080/gp2018/monatsAbrechnung.jsp?jahr=<%out.print(j);%>&monat=<%out.print(m);%>'">
                <td><%out.println(j);%></td>
                <td><%out.println(Aufenthalt.monatsNamen[m-1]);%></td>
                <td><%out.println(s);%></td>
                <td><%out.println(Aufenthalt.monatsAbrechnung(aufenthalte, j, m));%>€</td>
            </tr> <%
            }       
            }
            }
            %>
        </table>
        <br>
        <button type="button" onclick="parent.location='http://localhost:8080/gp2018/index.jsp'">Abmelden</button>
        </body>
</html>
