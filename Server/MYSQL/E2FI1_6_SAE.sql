-- MySQL Script generated by MySQL Workbench
-- Mon Jun 25 14:00:44 2018
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema hotel
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `hotel` ;

-- -----------------------------------------------------
-- Schema hotel
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `hotel` DEFAULT CHARACTER SET utf8 ;
USE `hotel` ;

-- -----------------------------------------------------
-- Table `hotel`.`Mitglieder`
-- --------------------------------------------------
DROP TABLE IF EXISTS `hotel`.`Mitglieder` ;
-- -
CREATE TABLE IF NOT EXISTS `hotel`.`Mitglieder` (
  `MitgliedID` INT NOT NULL AUTO_INCREMENT,
  `Vorname` VARCHAR(45) NULL,
  `Nachname` VARCHAR(45) NULL,
  `Email` VARCHAR(45) NULL,
  `PWHash` VARCHAR(45) NULL,
  `Strasse` VARCHAR(45) NULL,
  `PLZ` VARCHAR(45) NULL,
  `Ort` VARCHAR(45) NULL,
  PRIMARY KEY (`MitgliedID`))
ENGINE = InnoDB;

INSERT INTO `hotel`.`Mitglieder` (Vorname, Nachname, Email, PWHash, PLZ, Ort)
VALUES ('Roman', 'Auer', 'romanauer@test.de', '12345678', '77723', 'Gengenbach');
INSERT INTO `hotel`.`Mitglieder` (Vorname, Nachname, Email, PWHash, PLZ, Ort)
VALUES ('Tester', 'Test', 'test@test.de', '123456', '77123', 'Gengenbach');
INSERT INTO `hotel`.`Mitglieder` (Vorname, Nachname, Email, PWHash, PLZ, Ort)
VALUES ('User1', 'Test', 'user1@test.de', '123456', '77123', 'Gengenbach');
INSERT INTO `hotel`.`Mitglieder` (Vorname, Nachname, Email, PWHash, PLZ, Ort)
VALUES ('User2', 'Test', 'user2@test.de', '123456', '77123', 'Gengenbach');
INSERT INTO `hotel`.`Mitglieder` (Vorname, Nachname, Email, PWHash, PLZ, Ort)
VALUES ('User3', 'Test', 'user3@test.de', '123456', '77123', 'Gengenbach');


-- -----------------------------------------------------
-- Table `hotel`.`Aufenthalte`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hotel`.`Aufenthalte` ;

CREATE TABLE IF NOT EXISTS `hotel`.`Aufenthalte` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `gekommen` TIMESTAMP NULL,
  `gegangen` TIMESTAMP NULL,
  `MitgliedID` INT NOT NULL,
  PRIMARY KEY (`id`, `MitgliedID`),
  INDEX `fk_Zeiten_Mitglieder_idx` (`MitgliedID` ASC),
  CONSTRAINT `fk_Zeiten_Mitglieder`
    FOREIGN KEY (`MitgliedID`)
    REFERENCES `hotel`.`Mitglieder` (`MitgliedID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
INSERT INTO `hotel`.`Aufenthalte` (gekommen, gegangen, MitgliedID)
VALUES("2018-02-15 09:10:35", "2018-02-15 15:50:55", 1);
INSERT INTO `hotel`.`Aufenthalte` (gekommen, gegangen, MitgliedID)
VALUES("2018-02-17 17:10:35", "2018-02-17 18:50:05", 1);
INSERT INTO `hotel`.`Aufenthalte` (gekommen, gegangen, MitgliedID)
VALUES("2018-03-05 14:10:35", "2018-03-05 15:50:55", 1);
INSERT INTO `hotel`.`Aufenthalte` (gekommen, gegangen, MitgliedID)
VALUES("2018-02-15 09:10:35", "2018-02-15 15:50:55", 2);
INSERT INTO `hotel`.`Aufenthalte` (gekommen, gegangen, MitgliedID)
VALUES("2018-02-16 09:10:35", "2018-02-16 15:50:55", 2);
INSERT INTO `hotel`.`Aufenthalte` (gekommen, gegangen, MitgliedID)
VALUES("2018-02-17 09:10:35", "2018-02-17 15:50:55", 2);
INSERT INTO `hotel`.`Aufenthalte` (gekommen, gegangen, MitgliedID)
VALUES("2018-02-18 09:10:35", "2018-02-18 15:50:55", 2);
INSERT INTO `hotel`.`Aufenthalte` (gekommen, gegangen, MitgliedID)
VALUES("2018-03-01 09:10:35", "2018-03-01 15:50:55", 2);


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
