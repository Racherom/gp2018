/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Racherom
 */

import DAO.*;

// Test Aplication
public class main {
    public static void main(String[] args){
        System.out.println("Hallo Welt");
        
        MYSQLDAO mySqlDao = new MYSQLDAO();
        
        Mitglied m1 = mySqlDao.getMitglied("test@test.de");
        Mitglied m2 = mySqlDao.getMitglied("user1@test.de");
        
        Aufenthalt m1a1 = new Aufenthalt("24.06.2018 13:20:00", "24.06.2018 15:20:00", m1.getID());
        Aufenthalt m1a2 = new Aufenthalt("26.06.2018 13:20:00", "26.06.2018 15:20:00", m1.getID());
        Aufenthalt m2a1 = new Aufenthalt("24.06.2018 13:20:00", "24.06.2018 15:20:00", m2.getID());
        Aufenthalt m2a2 = new Aufenthalt("26.06.2018 13:20:00", "26.06.2018 15:20:00", m2.getID());
        
        mySqlDao.insertAufenthalt(m1a1);
        mySqlDao.insertAufenthalt(m1a2);
        mySqlDao.insertAufenthalt(m2a1);
        mySqlDao.insertAufenthalt(m2a2);
    }
}
