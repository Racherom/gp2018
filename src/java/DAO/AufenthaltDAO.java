/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.util.List;

/**
 *
 * @author roman.auer
 */
public interface AufenthaltDAO {
    public List<Aufenthalt> getAufenthalte(Mitglied m);
    public void insertAufenthalt(Aufenthalt a);
    public void updateAufenthalt(Aufenthalt a);
    public void deleteAufenthalt(Aufenthalt a);
}
