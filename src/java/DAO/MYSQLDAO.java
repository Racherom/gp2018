package DAO;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MYSQLDAO implements MitgliedDAO, AufenthaltDAO {
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";

    private Connection conn;
    public MYSQLDAO(){
        try {
            Class.forName(JDBC_DRIVER);
            conn = DriverManager.getConnection("jdbc:mysql://192.168.99.100:3306/hotel", "root", "toor");
        } catch (SQLException | ClassNotFoundException e){
            System.err.println(e.toString());
            e.printStackTrace();
        }
    }


    @Override
    public Mitglied getMitglied(String email) {
        Mitglied m = null;
        try {
            if(conn != null){
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM Mitglieder WHERE Email = ?");
            stmt.setString(1, email);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()){
                m = new Mitglied(rs.getInt("MitgliedID"), rs.getString("vorname"), rs.getString("nachname"), rs.getString("email"), rs.getString("pwhash"));
            }
            }

        } catch (SQLException se){
            System.err.println(se.toString());
            se.printStackTrace();
        }
        return m;
    }

    @Override
    public void insertMitglied(Mitglied m) {
        try{
            PreparedStatement stmt = conn.prepareStatement("INSERT INTO Aufenthalte (Vorname, Nachname, Email, PWHash) VALUES(?, ?, ?, ?)");
            stmt.setString(1, m.getVorname());
            stmt.setString(2, m.getNachname());
            stmt.setString(3, m.getEmail());
            stmt.setString(4, m.getPWHash());
            stmt.executeUpdate();
                    
        } catch (SQLException se){
            System.err.println(se.toString());
            se.printStackTrace();
        }
    }

    @Override
    public void updateMitglied(Mitglied m) {
        try{
            PreparedStatement stmt = conn.prepareStatement("UPDATE Mitglieder SET Vorname = ?, Nachname = ?, Email = ?, PWHash = ? WHERE MitgliederID = ?");
            stmt.setString(1, m.getVorname());
            stmt.setString(2, m.getNachname());
            stmt.setString(3, m.getEmail());
            stmt.setString(4, m.getPWHash());
            stmt.setInt(5, m.getID());
            stmt.executeUpdate();
                    
        } catch (SQLException se){
            System.err.println(se.toString());
            se.printStackTrace();
        }
    }

    @Override
    public void deleteMitglied(Mitglied m) {
        try{
            PreparedStatement stmt = conn.prepareStatement("DELETE FROM Mitglieder WHERE MitgliedID = ?");
            stmt.setInt(1, m.getID());
            stmt.executeUpdate();
            
        } catch (SQLException se){
            System.err.println(se.toString());
            se.printStackTrace();
        }
    }
    
    @Override
    public List<Aufenthalt> getAufenthalte(Mitglied m) {
        List<Aufenthalt> aufenthalte = new ArrayList<>();
        try{
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM Aufenthalte WHERE MitgliedID = ?");
            stmt.setInt(1, m.getID());
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                aufenthalte.add(new Aufenthalt(rs.getTimestamp("gekommen"), rs.getTimestamp("gegangen"), m.getID()));
            }
            for(int i = 0; i < aufenthalte.size(); i++){
                System.out.println(aufenthalte.get(i).getVon());
                System.out.println(aufenthalte.get(i).getBis());
            }
            
            rs.close();
        } catch (SQLException se){
            System.err.println(se.toString());
            se.printStackTrace();
        }
        return aufenthalte;
    }
   
    @Override
    public void insertAufenthalt(Aufenthalt a) {
        try{
            PreparedStatement stmt = conn.prepareStatement("INSERT INTO Aufenthalte (gekommen, gegangen, MitgliedID) VALUES(?, ?, ?)");
           
            stmt.setTimestamp(1, new java.sql.Timestamp(a.getVonDate().getTime()));
            stmt.setTimestamp(2, new java.sql.Timestamp(a.getBisDate().getTime()));
            stmt.setInt(3, a.getMitgliedID());
            stmt.executeUpdate();
        } catch (SQLException se){
            System.err.println(se.toString());
            se.printStackTrace();
        }
    }
   
    @Override
    public void updateAufenthalt(Aufenthalt a) {
        try{
            PreparedStatement stmt = conn.prepareStatement("UPDATE Aufenthalte SET Gekommen = ?, Gegangen = ?, MitgliedID = ? WHERE id = ?");
           
            stmt.setTimestamp(1, new java.sql.Timestamp(a.getVonDate().getTime()));
            stmt.setTimestamp(2, new java.sql.Timestamp(a.getBisDate().getTime()));
            stmt.setInt(3, a.getMitgliedID());
            stmt.setInt(4, a.getAufenthaltID());
            stmt.executeUpdate();
        } catch (SQLException se){
            System.err.println(se.toString());
            se.printStackTrace();
        }
    }

    @Override
    public void deleteAufenthalt(Aufenthalt a) {
        try{
           PreparedStatement stmt = conn.prepareStatement("DELETE FROM Aufenthalte WHERE id = ?");
            stmt.setInt(1, a.getAufenthaltID());
            stmt.executeUpdate(); 
        } catch (SQLException se){
            System.err.println(se.toString());
            se.printStackTrace();
        }
    }
    
    public void close(){
        try{
           conn.close();
        }catch (SQLException se){
            System.err.println(se.toString());
            se.printStackTrace();
        }
    }
}

