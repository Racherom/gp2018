package DAO;

public interface MitgliedDAO {
    public Mitglied getMitglied(String email);
    public void insertMitglied(Mitglied m);
    public void updateMitglied(Mitglied m);
    public void deleteMitglied(Mitglied m);
}
