package DAO;

import java.util.*;

public class Mitglied{
    private int id;
    private String vorname;
    private String nachname;
    private String email;
    private String pwHash;
    
    public Mitglied(){
        
    }

    public Mitglied(int id, String vorname, String nachname, String email, String pwHash) {
        this.id = id;
        this.vorname = vorname;
        this.nachname = nachname;
        this.email = email;
        this.pwHash = pwHash;
    }
    
    public String getVorname() {
        return vorname;
    }

    public void setVorame(String vorname) {
        this.vorname = vorname;
    }
    public String getNachname() {
        return nachname;
    }
    public void setNachame(String nachname) {
        this.nachname = nachname;
    }
    public int getID() {
        return id;
    }
    public String getEmail() {
        return email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }
    
    public void setPW(String pw){
        this.pwHash = hashPW(pw);
    }
    
    public String getPWHash(){
        return this.pwHash;
    }
    public static String hashPW(String pw){
        return pw;
    }
    
    public boolean checkPW(String pw) {
        return this.pwHash.equals(hashPW(pw));
    }
}
