package DAO;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Date;
import java.util.List;

public class Aufenthalt {
    public static String [] monatsNamen = {"Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"};
    private static DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy kk:mm:ss");
    private static DateFormat timeFormat = new SimpleDateFormat("kk:mm:ss");
    private int mitgliedID;
    private int aufenthaltID;
    private GregorianCalendar von = new GregorianCalendar();
    private GregorianCalendar bis = new GregorianCalendar();

    public Aufenthalt(GregorianCalendar von, GregorianCalendar bis, int mitgliedID){
        this.von = von;
        this.bis = bis;
        this.mitgliedID = mitgliedID;
    }
    
    public Aufenthalt(Date von, Date bis, int mitgliedID){
        this.von.setTime(von);
        this.bis.setTime(bis);
        this.mitgliedID = mitgliedID;
    }
    public Aufenthalt(int jahr, int monat, int tag, String von, String bis){
        try {
            this.von.setTime(timeFormat.parse(von));
            this.bis.setTime(timeFormat.parse(bis));
        } catch (ParseException e) {
            System.out.println(e.toString());
        }
        this.von.set(GregorianCalendar.YEAR, jahr);
        this.bis.set(GregorianCalendar.YEAR, jahr);
        this.von.set(GregorianCalendar.MONTH, monat-1);
        this.bis.set(GregorianCalendar.MONTH, monat-1);
        this.von.set(GregorianCalendar.DAY_OF_MONTH, tag-1);
        this.bis.set(GregorianCalendar.DAY_OF_MONTH, tag-1);
    }
    public Aufenthalt(int jahr, int monat, int tag, String von, String bis, int mitgliedID){
        try {
            this.von.setTime(timeFormat.parse(von));
            this.bis.setTime(timeFormat.parse(bis));
        } catch (ParseException e) {
            System.out.println(e.toString());
        }
        this.von.set(GregorianCalendar.YEAR, jahr);
        this.bis.set(GregorianCalendar.YEAR, jahr);
        this.von.set(GregorianCalendar.MONTH, monat-1);
        this.bis.set(GregorianCalendar.MONTH, monat-1);
        this.von.set(GregorianCalendar.DAY_OF_MONTH, tag-1);
        this.bis.set(GregorianCalendar.DAY_OF_MONTH, tag-1);
        this.mitgliedID = mitgliedID;
    }
    public Aufenthalt(String von, String bis, int mitgliedID) {
        try {
            this.von.setTime(dateFormat.parse(von));
            this.bis.setTime(dateFormat.parse(bis));
        } catch (ParseException e) {
            System.err.println(e.toString());
            e.printStackTrace();
        }
        this.mitgliedID = mitgliedID;
    }
    
    public void setMitgliedID(int id){
        mitgliedID = id;
    }
    
    public int getMitgliedID(){
        return mitgliedID;
    }
    
    public void setAufenthaltID(int id){
        aufenthaltID = id;
    }
    
    public int getAufenthaltID(){
        return aufenthaltID;
    }

    public int getJahr(){
        return von.get(GregorianCalendar.YEAR);
    }

    public int getMonat() {
        return von.get(GregorianCalendar.MONTH)+1;
    }

    public int getTag(){
        return von.get(GregorianCalendar.DAY_OF_MONTH)+1;
    }

    public float getStunden(){
        float s = (float)(bis.getTimeInMillis()-von.getTimeInMillis())/(float)(1000 * 60 * 60);
        System.out.println(s);
        return s;
    }

    public String getTimeVon(){
        return  timeFormat.format(von.getTime());
    }

    public String getTimeBis(){
        return  timeFormat.format(bis.getTime());
    }

    public String getVon() {
        return dateFormat.format(von.getTime());
    }

    public String getBis() {
        return dateFormat.format(bis.getTime());
    }
    
    public Date getVonDate() {
        return von.getTime();
    }
    
    public Date getBisDate() {
        return bis.getTime();
    }
    
    public static float stunden(List<Aufenthalt> a){
        float stunden = 0;
        for( int i = 0; i < a.size(); i++){
            stunden += a.get(i).getStunden();
        }
        return stunden;
    }
    
    public static float abrechnung(float stunden){
        float preis = 0;
        if (stunden > 50){
            preis += (stunden-50)*2;
            stunden = 50;
        }
        
        if (stunden > 10){
            preis += (stunden-10)*2.5;
            stunden = 10;
        }
        preis += stunden*3;
        return preis;
    }
    
    public static float abrechnung(List<Aufenthalt> a){
        return abrechnung(stunden(a));
    }
    
    public static float monatsAbrechnung(List<Aufenthalt> all, int year, int month ){
        return abrechnung(getMonth(all, year, month));
    }
    
    public static List<Aufenthalt> getMonth(List<Aufenthalt> all, int year, int month){
        List<Aufenthalt> result = new ArrayList<>();
        for (int i = 0; i < all.size(); i++){
            Aufenthalt a = all.get(i);
            System.out.println("Year: "+a.getJahr()+" Month: "+ a.getMonat());
            if(year == a.getJahr() && month == a.getMonat()){
                result.add(a);
            }
        }
        System.out.println("GetMonth size");
        System.out.println(result.size());
        return result;
    }
}